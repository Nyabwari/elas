import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.nyabwari.etta.Watchdog;

public class RestartService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context,Watchdog.class));
        context.startService(new Intent(context,NotificationService.class));
    }
}