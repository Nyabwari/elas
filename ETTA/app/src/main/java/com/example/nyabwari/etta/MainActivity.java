package com.example.nyabwari.etta;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    EditText victimName, victimMessage;
    Button txtVictim, callVictim, trackOnMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        callVictim=(Button) findViewById(R.id.btnCallvictim);

        victimName = (EditText) findViewById(R.id.vName);
        victimMessage = (EditText) findViewById(R.id.vMessage);
        callVictim = (Button) findViewById(R.id.btnCallvictim);
        trackOnMap = (Button) findViewById(R.id.btnTrack);

        callVictim.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Bundle extras = getIntent().getExtras();
                    String body = extras.getString("text");

                    String phNo = body.substring(0, body.indexOf("#"));
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phNo, null));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(MainActivity.this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        trackOnMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle extras = getIntent().getExtras();
                String body = extras.getString("text");
                Log.d("onClick: ",body );
                String cdts=body.substring(body.indexOf("}")+1);
               // String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", cdts.substring(0, cdts.indexOf(",")), cdts.substring(cdts.indexOf(",")+1), "Victim Location");

                Uri gmmIntentUri = Uri.parse("google.navigation:q="+cdts);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        Bundle extras = getIntent().getExtras();
        String body = extras.getString("text");

        String name=body.substring( body
                .indexOf("#")+1,body.indexOf("*"));

        String message=body.substring(body.indexOf("*")+1, body.indexOf("}"));



        victimName.setText(name);
        victimMessage.setText(message);


    }
}
