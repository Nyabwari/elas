package com.example.nyabwari.etta;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


public class SMSChecker extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_READ_CONTACTS = 100;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    String Body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smschecker);

        startTimer();
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            showContacts();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, PERMISSION_REQUEST_READ_CONTACTS);
        }
    }
    private void stopTimer(){
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
    }
    private void startTimer(){
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run(){
                        showContacts();
                    }
                });
            }
        };
        timer.schedule(timerTask, 5000, 5000);
    }

    private void showContacts() {
        Uri inboxURI = Uri.parse("content://sms/inbox");
        ContentResolver cr = getContentResolver();


        Cursor c = cr.query(inboxURI, null, null, null, null);
        while (c.moveToNext()) {
            String Number = c.getString(c.getColumnIndexOrThrow("address")).toString();
            Body = c.getString(c.getColumnIndexOrThrow("body")).toString();

            if(Number.equals("0748285888")||Number.equals("254748285888")||Number.equals("+254748285888"))
            {
                showNotification();
                timer.cancel();
                Intent i = new Intent();
                i.setClassName("com.example.nyabwari.etta", "com.example.nyabwari.etta.MainActivity");
                i.putExtra("text",Body);

                startActivity(i);
            }
        }
        c.close();


    }
    public void showNotification() {
        try {


            String message = Body.substring(Body.indexOf("*") + 1, Body.indexOf("}"));
            PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, SMSChecker.class), 0);
            Resources r = getResources();
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Notification notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("NEW EMERGENCY ALERT")
                    .setContentText(message)
                    .setContentIntent(pi)
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
        catch (Exception e)
        {
            Toast.makeText(SMSChecker.this, "Bad Sms Formart.", Toast.LENGTH_SHORT).show();
            finish();

        }

    }

}